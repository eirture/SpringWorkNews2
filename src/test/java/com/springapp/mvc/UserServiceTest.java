package com.springapp.mvc;

import com.springapp.mvc.dao.UserDaoImpl;
import com.springapp.mvc.db.DbConnect;
import com.springapp.mvc.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by Jie on 2015-12-08.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml", "file:src/main/webapp/WEB-INF/classes/hibernate.cfg.xml"})
public class UserServiceTest {

    @Autowired
    UserDaoImpl mUserDao;


    @Test
    public void registTest() {
        DbConnect.getSession();
        System.out.println("test");
    }


}
