package com.springapp.mvc.intercepter;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Repository;

/**
 * Created by Jie on 2015-12-11.
 */
@Repository
@Aspect
public class SessionAspect {


    @Before("within(com.springapp.mvc.services.UserService)")
    public void openSession() {
        System.out.println("aspect open Session!");
    }

    @After("target(com.springapp.mvc.services.UserService)")
    public void closeSession() {
        System.out.println("aspect close Session!");
    }
}
