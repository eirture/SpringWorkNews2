package com.springapp.mvc.intercepter;

import com.springapp.mvc.annotation.UnAuthorization;
import com.springapp.mvc.commont.Constant;
import com.springapp.mvc.model.UserEntity;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * Created by Jie on 2015-12-05.
 */
public class MyIntercepter extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("preHandle");
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();

        if (method.getAnnotation(UnAuthorization.class) != null) {
            return true;
        }

        UserEntity user = (UserEntity) request.getSession().getAttribute(Constant.USER);

        if (user == null) {
            System.out.println("请求拦截");
//            response.setStatus(HttpServletResponse.SC_OK);
//            return false;
        }
        System.out.println("perHandle 拦截通过");

        String userid = user == null ? "9" : user.getId() + "";

        request.setAttribute(Constant.CURRENT_USER_ID, userid);//第二个参数改为user.getid();

        return true;
    }
}
