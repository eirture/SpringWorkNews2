package com.springapp.mvc.services;

import com.springapp.mvc.commont.ListUtils;
import com.springapp.mvc.dao.ManagerDaoImpl;
import com.springapp.mvc.db.DbConnect;
import com.springapp.mvc.model.ManagersEntity;
import com.springapp.mvc.model.UserEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Jie on 2015-12-12.
 */
@Service
public class ManagerServices {

    @Autowired
    ManagerDaoImpl managerDao;

    public boolean isManager(String id) {
        return managerDao.query(id) != null;
    }

    /**
     * @param id   据此找到管理员
     * @param rank 可以修改的内容
     * @return 修改后的对象
     */
    public ManagersEntity updateRank(String id, String rank) {
        ManagersEntity manager = managerDao.query(id);
        manager.setRank(rank);

        managerDao.update(manager);
        return manager;
    }

    public ManagersEntity updatePasswd(String id, String passwd) {
        ManagersEntity manager = managerDao.query(id);
        manager.setPasswd(passwd);

        managerDao.update(manager);
        return manager;
    }

    public ManagersEntity login(String email, String passwd) {

        Session mSession = DbConnect.getSession();
        Query query = mSession.createQuery("from UserEntity where email=:e");
        query.setString("e", email);
        if (ListUtils.isEmpty(query.list())) {
            return null;
        }
        UserEntity user = (UserEntity) query.list().get(0);
        Query query1 = mSession.createQuery("from ManagersEntity where id=:i and passwd=:p");
        query1.setInteger("i", user.getId());
        query1.setString("p", passwd);
        List ms = query1.list();
        if (ListUtils.isEmpty(ms))
            return null;
        ManagersEntity manager = (ManagersEntity) ms.get(0);
        mSession.close();

        return manager;
    }

}
