package com.springapp.mvc.services;

import com.springapp.mvc.dao.UserDaoImpl;
import com.springapp.mvc.db.DbConnect;
import com.springapp.mvc.model.UserEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Jie on 2015-12-02.
 */

@Service
public class UserService {

    @Autowired
    UserDaoImpl mUserDao;

    public UserEntity login(String username, String passwd) {
        Session mSession = DbConnect.getSession();

        List<UserEntity> result;
        try {
            Query query = mSession.createQuery("from UserEntity where username = ? or email= ? and passwd = ?");
            query.setString(0, username);
            query.setString(1, username);
            query.setString(2, passwd);
            result = query.list();
        } finally {
            mSession.close();
        }
        if (result.size() == 0)
            return null;
        return result.get(0);
    }

    public int findIdByEmail(String email) {
        Session mSession = DbConnect.getSession();

        Query query = mSession.createQuery("from UserEntity where email =:e");
        query.setString("e", email);
        List<UserEntity> users = query.list();
        mSession.close();
        if (users.size() == 0)
            return -1;
        UserEntity user = users.get(0);
        return user.getId();
    }

    public UserEntity regist(String icon, String username, String eamil, String passwd) {

        UserEntity user = new UserEntity();

        System.out.println("regist user icon:" + icon);

        user.setIcon(icon);
        user.setUsername(username);
        user.setEmail(eamil);
        user.setPasswd(passwd);

        mUserDao.insert(user);

        return user;
    }


    public UserEntity update(String icon, String username, String passwd, String id) {
        UserEntity user = mUserDao.query(id);
        user.setIcon(icon);
        user.setUsername(username);
        user.setPasswd(passwd);
        mUserDao.update(user);

        return user;
    }

    public void deleteUser(String id) {
        mUserDao.delete(id);
    }

    public UserEntity findUserById(String id) {
        return mUserDao.query(id);
    }


}
