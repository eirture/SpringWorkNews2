package com.springapp.mvc.services;

import com.springapp.mvc.dao.ArticleDaoImpl;
import com.springapp.mvc.db.DbConnect;
import com.springapp.mvc.model.ArticleEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jie on 2015-12-10.
 */
@Service
public class ArticleService {


    @Autowired
    ArticleDaoImpl mArticleDao;

    public void add(ArticleEntity article) {
        mArticleDao.insert(article);
    }

    public ArticleEntity queryByid(String id) {
        return mArticleDao.query(id);
    }

    public void delete(int id) {
        mArticleDao.delete(String.valueOf(id));
    }


    public List<ArticleEntity> query(Map<String, String> param) {
        return mArticleDao.queryAll(param, false);
    }

    public List<ArticleEntity> queryByTime(String start, String end) {

        System.out.println("time" + start + " , " + end);
        Timestamp startTime = Timestamp.valueOf(start);
        Timestamp endTime = Timestamp.valueOf(end);

        return mArticleDao.queryByTime(startTime, endTime);
    }

    public List<ArticleEntity> queryByPage(int begin, int pageSize, boolean isManager) {
        Session mSession = DbConnect.getSession();
        String sql = "from ArticleEntity where ";
        if (!isManager) {
            sql += " pass= 1";
        }
        Query query = mSession.createQuery(sql);
        query.setFirstResult(begin);
        query.setMaxResults(pageSize);
        List<ArticleEntity> pageData = query.list();
        mSession.close();
        return pageData;
    }

    public void updateArticle(ArticleEntity article) {
        mArticleDao.update(article);
    }

}
