package com.springapp.mvc.services;

import com.springapp.mvc.db.DbConnect;
import com.springapp.mvc.model.ArticleEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Jie on 2015-12-09.
 */
@Service
public class ArticlePagesServices {

    private Session mSession;

    public List<ArticleEntity> getPageList(int userid, int begin, int length) {

        mSession = DbConnect.getSession();
        Query query = mSession.createQuery("select ArticleEntity from ArticleEntity where id=:u");
        query.setString("u", String.valueOf(userid));
        query.setMaxResults(length);
        query.setFirstResult(begin);

        mSession.close();
        return query.list();
    }

    public List<ArticleEntity> getPageByStatus(int status, int begin, int length) {
        return null;
    }

}
