package com.springapp.mvc.dao;

import com.springapp.mvc.db.DbConnect;
import com.springapp.mvc.model.ManagersEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by Jie on 2015-12-12.
 */

@Repository
public class ManagerDaoImpl implements BaseDao<ManagersEntity> {

    private Session mSession;

    @Override
    public void insert(ManagersEntity managersEntity) {
//        mSession = DbConnect.getSession();
//        mSession.beginTransaction();
//
//        mSession.save(managersEntity);
//        mSession.getTransaction().commit();
//        mSession.close();
        //管理员不允许注册，手动加入数据库
    }

    @Override
    public boolean update(ManagersEntity managersEntity) {
        mSession = DbConnect.getSession();
        mSession.beginTransaction();

        mSession.update(managersEntity);

        mSession.getTransaction().commit();
        mSession.close();
        return true;
    }

    @Override
    public ManagersEntity query(String keys) {

        ManagersEntity managersEntity = null;
        try {
            mSession = DbConnect.getSession();
            Query query = mSession.createQuery("from ManagersEntity where id =:id");
            query.setString("id", keys);
            if (query.list().size() > 0)
                managersEntity = (ManagersEntity) query.list().get(0);
        } finally {
            mSession.close();
        }

        return managersEntity;
    }

    @Override
    public boolean delete(String key) {

        mSession = DbConnect.getSession();
        mSession.beginTransaction();
        Query query = mSession.createQuery("delete ManagersEntity where id=:id");
        query.setString("id", key);
        mSession.close();

        return true;
    }
}
