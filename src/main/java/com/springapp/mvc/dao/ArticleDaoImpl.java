package com.springapp.mvc.dao;

import com.springapp.mvc.commont.ListUtils;
import com.springapp.mvc.db.DbConnect;
import com.springapp.mvc.model.ArticleEntity;
import com.springapp.mvc.model.UserEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Jie on 2015-12-10.
 */

@Repository
public class ArticleDaoImpl implements BaseDao<ArticleEntity> {

    private Session mSession;

    @Override
    public void insert(ArticleEntity articleEntity) {
        mSession = DbConnect.getSession();

        Transaction tx = mSession.beginTransaction();
        try {
            mSession.save(articleEntity);
            System.out.println("article id:" + "" + " , " + articleEntity.getArticleId());
            tx.commit();
        } finally {
            mSession.close();
        }
    }

    @Override
    public boolean update(ArticleEntity articleEntity) {
        mSession = DbConnect.getSession();
        Transaction tx = mSession.beginTransaction();
        try {
            mSession.update(articleEntity);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return false;
        } finally {
            mSession.close();
        }
        return true;
    }

    public List<ArticleEntity> queryAll(Map<String, String> param, boolean isLike) {
        mSession = DbConnect.getSession();
        String sql = "from ArticleEntity where 1=1";

        Iterator<String> keys = param.keySet().iterator();
        String key;
        while (keys.hasNext()) {
            key = keys.next();
            if (isLike) {
                sql += " and " + key + " like %" + param.get(key) + "%";
            } else {
                sql += " and " +
                        key + " = " + param.get(key);
            }
        }
        System.out.println("query all sql: " + sql);
        Query query = mSession.createQuery(sql);

        List<ArticleEntity> articles = query.list();
        mSession.close();

        return articles;

    }

    @Override
    public ArticleEntity query(String key) {

        mSession = DbConnect.getSession();
        ArticleEntity articleEntity = null;

        try {
            Query mQuery = mSession.createQuery("from ArticleEntity where id =:id ");
            mQuery.setString("id", key);

            List<ArticleEntity> articles = mQuery.list();
            if (!ListUtils.isEmpty(articles)) {
                articleEntity = articles.get(0);
            }
        } finally {
            mSession.close();
        }
        return articleEntity;
    }

    @Override
    public boolean delete(String key) {
        mSession = DbConnect.getSession();
        Transaction tx = mSession.beginTransaction();
        try {

            Query query = mSession.createQuery("delete ArticleEntity where id =:id");
            query.setString("id", key);
            query.executeUpdate();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            return false;
        } finally {
            mSession.close();
        }
        return true;
    }


    public List<ArticleEntity> queryByTime(Timestamp start, Timestamp end) {
        mSession = DbConnect.getSession();

        Query query = mSession.createQuery("from ArticleEntity where createTime between ? and ?");
        query.setTimestamp(0, start);
        query.setTimestamp(1, end);
        List<ArticleEntity> articles = query.list();
        mSession.close();
        return articles;
    }

}
