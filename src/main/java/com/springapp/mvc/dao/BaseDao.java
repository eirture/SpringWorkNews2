package com.springapp.mvc.dao;

import java.util.Map;

/**
 * Created by Jie on 2015-12-01.
 */
public interface BaseDao<T> {

    public void insert(T t);

    public boolean update(T t);

    public T query(String keys);

    public boolean delete(String key);
}
