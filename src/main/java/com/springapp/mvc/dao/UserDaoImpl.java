package com.springapp.mvc.dao;

import com.springapp.mvc.commont.ListUtils;
import com.springapp.mvc.db.DbConnect;
import com.springapp.mvc.model.UserEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Jie on 2015-12-01.
 */

@Repository
public class UserDaoImpl implements BaseDao<UserEntity> {


    private Session mSession;

    @Override
    public void insert(UserEntity userEntity) {

        mSession = DbConnect.getSession();
        Transaction tx = mSession.beginTransaction();
        try {
            Integer id = (Integer) mSession.save(userEntity);
            System.out.println("Regist user id:" + id + " , " + userEntity.getId());
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        } finally {
            mSession.close();
        }

    }

    @Override
    public boolean update(UserEntity userEntity) {
        mSession = DbConnect.getSession();
        Transaction tx = mSession.beginTransaction();
        try {
            mSession.update(userEntity);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return false;
        } finally {
            if (mSession != null && mSession.isOpen()) {
                mSession.close();
            }
        }
        return true;
    }

    /**
     * 查询接口
     *
     * @param id userid
     * @return
     */
    @Override
    public UserEntity query(String id) {

        mSession = DbConnect.getSession();
        UserEntity user = null;

        try {
            Query mQuery = mSession.createQuery("from UserEntity where id=:d");
            mQuery.setString("d", id);

            List<UserEntity> users = mQuery.list();
            if (!ListUtils.isEmpty(users)) {
                user = users.get(0);
            }
        } finally {
            mSession.close();
        }

        return user;
    }


    public boolean checkPerson4Name(String username) {

        boolean result = false;

        try {
            mSession = DbConnect.getSession();
            Query mQuery = mSession.createQuery("from UserEntity where username = ?");
            mQuery.setString(0, username);
            result = mQuery.list().isEmpty();
        } finally {
            mSession.close();
        }

        return result;
    }

    public boolean checkEmailIsExist(String eamil) {

        boolean result = false;
        mSession = DbConnect.getSession();
        try {
            Query mQuery = mSession.createQuery("from UserEntity where email = ?");
            mQuery.setString(0, eamil);
            result = mQuery.list().isEmpty();
        } finally {
            mSession.close();
        }

        return result;
    }


    public UserEntity findById(int id) {
        mSession = DbConnect.getSession();
        UserEntity user = (UserEntity) mSession.get(UserEntity.class, id);
        mSession.close();
        return user;
    }

    /**
     * @param key userId
     * @return
     */
    @Override
    public boolean delete(String key) {
        mSession = DbConnect.getSession();
        Transaction tx = mSession.beginTransaction();
        try {

            Query query = mSession.createQuery("delete UserEntity where id =:id");
            query.setString("id", key);
            query.executeUpdate();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            return false;
        } finally {
            mSession.close();
        }
        return true;
    }
}
