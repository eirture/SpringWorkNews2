package com.springapp.mvc.model.user;

import com.springapp.mvc.model.ManagersEntity;
import com.springapp.mvc.model.UserEntity;

import java.sql.Timestamp;

/**
 * Created by Jie on 2015-12-12.
 */
public class Manager {
    public int id;
    public String username;
    public String email;
    public String mPasswd;
    public Timestamp registDate;
    public String rank;

    public Manager(UserEntity u, ManagersEntity m) {
        id = u.getId();
        username = u.getUsername();
        email = u.getEmail();
        mPasswd = m.getPasswd();
        registDate = u.getRegistDate();
        rank = m.getRank();
    }
}
