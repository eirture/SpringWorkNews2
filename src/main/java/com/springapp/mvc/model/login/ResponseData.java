package com.springapp.mvc.model.login;

/**
 * Created by Jie on 2015-12-12.
 */
public class ResponseData {
    public String status;
    public Object data;

    public ResponseData(String status, Object data) {
        this.status = status;
        this.data = data;
    }
}
