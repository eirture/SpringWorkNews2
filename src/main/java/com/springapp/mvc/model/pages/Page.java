package com.springapp.mvc.model.pages;

import java.util.List;

/**
 * Created by Jie on 2015-12-09.
 */
public class Page<T> {

    // 1.每页显示数量(everyPage)
    public int everyPage;
    // 2.总记录数(totalCount)
    public int totalCount;
    // 3.总页数(totalPage)
    public int totalPage;
    // 4.当前页(currentPage)
    public int currentPage;
    // 5.起始点(beginIndex)
    public int beginIndex;

    public List<T> pageDatas;


    public Page(int everyPage, int totalCount, int currentPage) {
        this.everyPage = everyPage;
        this.totalCount = totalCount;
        this.currentPage = currentPage;
        init();
    }

    private void init() {
        totalPage = totalCount / everyPage + (totalCount % everyPage == 0 ? 0 : 1);
        beginIndex = (currentPage - 1) * everyPage;
    }

}
