package com.springapp.mvc.controller;

import com.springapp.mvc.annotation.UnAuthorization;
import com.springapp.mvc.commont.Constant;
import com.springapp.mvc.model.UserEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Jie on 2015-12-10.
 */
@Controller
@RequestMapping("/news")
public class HtmlController {

    @RequestMapping("/**")
    @UnAuthorization
    public String html(HttpServletRequest request) {
        System.out.println("controller html");
        String url = request.getRequestURI();
        UserEntity user = (UserEntity) request.getSession().getAttribute(Constant.USER);
        if (user != null || (url.contains("login") || url.contains("mainPage") || url.contains("show-article") || url.contains("regist"))) {
            return url.replace("/news/", "/");

        }
        return "hello";
//        return url.replace("/news/", "/");
    }

}
