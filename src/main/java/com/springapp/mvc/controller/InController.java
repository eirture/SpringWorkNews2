package com.springapp.mvc.controller;

import com.springapp.mvc.annotation.UnAuthorization;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Jie on 2015-12-10.
 */
@Controller
@RequestMapping("/")
public class InController {

    @RequestMapping
    @UnAuthorization
    public String in() {
        System.out.println("test");
        return "mainPage";
    }
}
