package com.springapp.mvc.controller;

import com.springapp.mvc.commont.ResponseTools;
import com.springapp.mvc.commont.ResponseWapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/upload")
public class UpLoadController {

    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    @ResponseBody
    public String uploadFile(@RequestParam("files") MultipartFile file, HttpServletRequest request) {
        String path = up(file, request);
        return ResponseWapper.getResponse(path != null, path);
    }

    private boolean saveFile(String filePath, MultipartFile file) {

        if (!file.isEmpty()) {
            try {
                File saveDir = new File(filePath);
                if (!saveDir.getParentFile().exists())
                    saveDir.getParentFile().mkdirs();
                file.transferTo(saveDir);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;

    }


    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
    @ResponseBody
    public String uploadImage(@RequestParam("files") MultipartFile file, HttpServletRequest request) {

        System.out.println("upImage:");

        String path = up(file, request);
        boolean isok = path != null;
        Map<String, String> result = new HashMap<String, String>();
        result.put("success", isok ? "1" : "0");
        result.put("message", isok ? "成功" : "失败");
        result.put("url", path);
        String re = ResponseTools.bean2Json(result);
        System.out.println("upImage finish:" + re);
        return re;
    }

    public String up(MultipartFile file, HttpServletRequest request) {
        if (file == null) {
            return null;
        }

        String filename = file.getOriginalFilename();
        String extname = filename.substring(filename.lastIndexOf("."));

        String newfilename = UUID.randomUUID().toString().replace("-", "") + extname;
        String filePath = request.getSession().getServletContext()
                .getRealPath("/") + "img\\head\\" + newfilename;
        String restPath = "img/head/" + newfilename;
        System.out.print(filePath);
        boolean isok = saveFile(filePath, file);
        return isok ? restPath : null;
    }

}
