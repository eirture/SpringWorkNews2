package com.springapp.mvc.controller;

import com.springapp.mvc.annotation.CurrentUser;
import com.springapp.mvc.commont.Classify;
import com.springapp.mvc.commont.Constant;
import com.springapp.mvc.commont.JsonUtil;
import com.springapp.mvc.commont.ResponseWapper;
import com.springapp.mvc.model.ArticleEntity;
import com.springapp.mvc.model.ManagersEntity;
import com.springapp.mvc.model.UserEntity;
import com.springapp.mvc.model.pages.Page;
import com.springapp.mvc.services.ArticleService;
import com.springapp.mvc.services.ManagerServices;
import com.springapp.mvc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Jie on 2015-12-10.
 */
@Controller
@RequestMapping(value = "/article", produces = "text/html;charset=UTF-8")
public class ArticleController {

    @Autowired
    ArticleService mService;
    @Autowired
    UserService mUserService;
    @Autowired
    ManagerServices managerServices;

    /**
     * @param article {
     *                'title':'',
     *                'icon':'',
     *                'classify':'',
     *                'content':''
     *                }
     * @param user
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public String createArticle(@RequestBody String article, @CurrentUser UserEntity user) {
        ArticleEntity art = new ArticleEntity();

        System.out.println("article add:" + article);

        String[] values = JsonUtil.getStringValue(article, Constant.TITLE, Constant.ICON, Classify.CLASSIFY, Constant.CONTENT);
        System.out.println("userid:" + user.getUsername());
        art.setUserid(user.getId());
        art.setTitle(values[0]);
        art.setIcon(values[1]);
        art.setClassify(values[2]);
        art.setContent(values[3]);
        mService.add(art);

        return ResponseWapper.getResponse(true, art);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String deleteArticleById(@PathVariable("id") int articleid) {
        System.out.println("deltet Article id:" + articleid);
        mService.delete(articleid);
        return ResponseWapper.getResponse(true, null);
    }

    @RequestMapping(value = "/findArticle/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String findArticleById(@PathVariable("id") String articleId) {
        ArticleEntity art = mService.queryByid(articleId);
        return ResponseWapper.getResponse(true, art);
    }


    /**
     * @param time {
     *             begin:"2015-10-10 00:00:00",
     *             end:"2015-10-11 23:59:59"
     *             }
     * @return
     */

    @RequestMapping("/findA4Time")
    @ResponseBody
    public String getArticles4Time(@RequestBody String time) {

        String[] values = JsonUtil.getStringValue(time, "begin", "end");
        List<ArticleEntity> arts = mService.queryByTime(values[0], values[1]);
        return ResponseWapper.getResponse(true, arts);
    }

    /**
     * @param page 从1开始
     * @param user
     * @return
     */
    @RequestMapping(value = "/getPageArticle/{page}", method = RequestMethod.GET)
    @ResponseBody
    public String getCurrentArticlePage(@PathVariable String page, @CurrentUser UserEntity user) {
        Map<String, String> param = new HashMap<String, String>();
        param.put(Constant.USER_ID, String.valueOf(user.getId()));
        List<ArticleEntity> articles = mService.query(param);

        Page<ArticleEntity> pageU = new Page<ArticleEntity>(5, articles.size(), Integer.parseInt(page));

        pageU.pageDatas = mService.queryByPage(
                pageU.beginIndex, 5, managerServices.isManager(String.valueOf(user.getId())));
        return ResponseWapper.getResponse(true, pageU);
    }

    /**
     * @param datas {
     *              'articleid':'',
     *              'ispass':'0/1'
     *              }
     * @return
     */
    @RequestMapping("/pass")
    @ResponseBody
    public String passArticle(@RequestBody String datas, @CurrentUser UserEntity user) {

        if (!managerServices.isManager(String.valueOf(user.getId()))) {
            return ResponseWapper.getResponse(false, user.getUsername() + " is not Manager");
        }

        String[] values = JsonUtil.getStringValue(datas, Constant.ARTICLE_ID, Constant.IS_PASS);

        ArticleEntity art = mService.queryByid(values[0]);
        art.setPass(Integer.parseInt(values[1]));
        mService.updateArticle(art);
        return ResponseWapper.getResponse(true, art);

    }


    @RequestMapping("/allclassify")
    @ResponseBody
    public String getAllClassify() {
        return ResponseWapper.getResponse(true, Classify.ALL_CLASSIFY);
    }


    /**
     * @param datas {
     *              'classify':''
     *              }
     * @return
     */
    @RequestMapping(value = "/getArticleByClassify", method = RequestMethod.POST)
    @ResponseBody
    public String getArticleByClassify(@RequestBody String datas) {
        String[] values = JsonUtil.getStringValue(datas, Classify.CLASSIFY);

        Map<String, String> param = new HashMap<String, String>();
        param.put(Classify.CLASSIFY, values[0]);
        param.put(Constant.PASS, "1");

        List<ArticleEntity> arts = mService.query(param);

        for (ArticleEntity a :
                arts) {
            System.out.println("content: " + a.getContent());
        }

        String rJson = ResponseWapper.getResponse(true, arts);
        System.out.println("rjson: " + rJson);
        return rJson;

    }

    @RequestMapping(value = "/getArticleByName/{name}", method = RequestMethod.GET)
    @ResponseBody
    public String getArticleByName(@PathVariable("name") String title) {


        Map<String, String> param = new HashMap<String, String>();
        param.put(Constant.ARTICLE_TITLE, title);

        List<ArticleEntity> arts = mService.query(param);

        return ResponseWapper.getResponse(true, arts);

    }

    @RequestMapping(value = "/saveid/{id}", method = RequestMethod.GET)
    public void saveid(@PathVariable String id, HttpServletRequest request) {
        request.getSession().setAttribute(Constant.ID_SAVE, id);
    }

    @RequestMapping(value = "/getSaveid", method = RequestMethod.GET)
    @ResponseBody
    public String getSaveId(HttpServletRequest request) {
        String id = (String) request.getSession().getAttribute(Constant.ID_SAVE);
        System.out.println("read :" + id);

        ArticleEntity article = null;
        if (id == null) {
            article = mService.queryByid(id);
        }
        return ResponseWapper.getResponse(id == null, article);

    }
}
