package com.springapp.mvc.controller;

import com.springapp.mvc.annotation.CurrentUser;
import com.springapp.mvc.annotation.UnAuthorization;
import com.springapp.mvc.commont.*;
import com.springapp.mvc.model.ManagersEntity;
import com.springapp.mvc.model.UserEntity;
import com.springapp.mvc.model.user.Manager;
import com.springapp.mvc.services.ManagerServices;
import com.springapp.mvc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Jie on 2015-11-26.
 */

@Controller
@RequestMapping(value = "/user", produces = "text/html;charset=UTF-8")
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    ManagerServices managerServices;


    /**
     * @param logininfo {
     *                  'username':''
     *                  'passwd':''
     *                  }
     * @param request
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    @UnAuthorization
    public String login(@RequestBody String logininfo, HttpServletRequest request) {

        String[] value = JsonUtil.getStringValue(
                logininfo, Constant.USER_NAME, Constant.PASSWD);

        UserEntity user = userService.login(value[0], value[1]);

        if (user != null) {
            request.getSession().setAttribute(Constant.USER, user);
            System.out.println("login success");
        }
        return ResponseWapper.getResponse(user == null ? false : true, user);
    }

    @RequestMapping(value = "/getCurrentUser", method = RequestMethod.GET)
    @ResponseBody
    public String getCurrentUser(HttpServletRequest request, @CurrentUser UserEntity user) {
        return ResponseWapper.getResponse(true, user);
    }

    @RequestMapping(value = "/equary/{userid}", method = RequestMethod.GET)
    @ResponseBody
    public Object equary(@PathVariable("userid") String userid) {
        System.out.println("equary:" + userid);

        UserEntity user = userService.findUserById(userid);

        return ResponseWapper.getResponse(true, user);
    }

    /**
     * @param regist {
     *               'icon','';
     *               "username":"",
     *               "email":"",
     *               "passwd":""
     *               }
     * @return
     */
    @RequestMapping(value = "/regist", method = RequestMethod.POST)
    @ResponseBody
    public Object regist(@RequestBody String regist) {
        boolean success = false;
        UserEntity user = null;
        String[] values = null;
        System.out.println(regist);
        try {
            values = JsonUtil.getStringValue(regist, Constant.ICON, Constant.USER_NAME, Constant.EMAIL, Constant.PASSWD);
            if (userService.findIdByEmail(values[1]) != -1) {
                return ResponseWapper.getResponse(false, values[1] + " is already exist ");
            }
            user = userService.regist(values[0], values[1], values[2], values[3]);
            success = true;
        } catch (Exception e) {
            success = false;
            e.printStackTrace();
        }
        return ResponseWapper.getResponse(success, user);
    }


    /**
     * @param validate {
     *                 'email':''
     *                 }
     * @return
     */
    @RequestMapping(value = "/validate", method = RequestMethod.POST)
    @ResponseBody
    public String validationExist(@RequestBody String validate) {

        System.out.println("validate email:" + validate);
        String[] values = JsonUtil.getStringValue(validate, Constant.EMAIL);
        int id = userService.findIdByEmail(values[0]);
        return ResponseWapper.getResponse(true, id >= 0);
    }

    /**
     * @param update {
     *               'icon':'',
     *               "username":"",
     *               "passwd":""
     *               }
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateUser(@RequestBody String update, @CurrentUser UserEntity user) {

        System.out.println("update:" + "update begin" + update);
        String[] values = JsonUtil.getStringValue(update, Constant.ICON, Constant.USER_NAME, Constant.PASSWD);

        UserEntity u = userService.update(values[0], values[1], values[2], String.valueOf(user.getId()));
        System.out.println("update:" + "update success");
        return ResponseWapper.getResponse(true, u);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String delete(@PathVariable("id") String id) {
        userService.deleteUser(id);
        return ResponseWapper.getResponse(true, null);
    }

    /**
     * @param login {
     *              "email":"",
     *              "passwd":""
     *              }
     * @return
     */
    @RequestMapping("/managerlogin")
    @ResponseBody
    public String managerLogin(@RequestBody String login, HttpServletRequest request) {

        String[] values = JsonUtil.getStringValue(login, Constant.EMAIL, Constant.PASSWD);

        ManagersEntity manager = managerServices.login(values[0], values[1]);
        Manager m = null;
        if (manager != null) {

            UserEntity user = userService.findUserById(String.valueOf(manager.getUserid()));

            if (user != null) {
                request.getSession().setAttribute(Constant.USER, user);
            }
            m = new Manager(user, manager);

            HttpSession session = request.getSession();
            session.setAttribute(Constant.MANAGER, manager);
            session.setAttribute(Constant.USER, user);
        }

        return ResponseWapper.getResponse(m != null, m);
    }

    @RequestMapping(value = "/islogin", method = RequestMethod.GET)
    @ResponseBody
    @UnAuthorization
    public String isLogin(HttpServletRequest request) {
        UserEntity user = (UserEntity) request.getSession().getAttribute(Constant.USER);
        return ResponseWapper.getResponse(user != null, user);
    }

    @RequestMapping("/loginout")
    @ResponseBody
    public String loginOut(HttpServletRequest request) {
        request.getSession().removeAttribute(Constant.USER);
        System.out.println("退出登录成功！");
        return ResponseWapper.getResponse(true, "退出成功");
    }
}
