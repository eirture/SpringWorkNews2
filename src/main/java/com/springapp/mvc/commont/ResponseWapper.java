package com.springapp.mvc.commont;

import com.springapp.mvc.model.login.ResponseData;

/**
 * Created by Jie on 2015-12-12.
 */
public class ResponseWapper {

    /**
     * @param pass     是否成功获取数据
     * @param response 返回的数据
     * @return
     */
    public static String getResponse(boolean pass, Object response) {
        ResponseData data = new ResponseData(pass ? StatusUtil.PASS : StatusUtil.FAIL, response);

        return ResponseTools.bean2Json(data);

    }

}
