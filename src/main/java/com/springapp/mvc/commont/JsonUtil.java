package com.springapp.mvc.commont;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by Jie on 2015-12-10.
 */
public class JsonUtil {

    /**
     * 自动获取json中的数据，仅适用于小型json数据
     *
     * @param json  原json数据
     * @param value key数组
     * @return 返回key数组对应取到的value
     */
    public static String[] getStringValue(String json, String... value) {
        String[] values = new String[value.length];
        JsonParser mParser = new JsonParser();
        JsonObject mJson = mParser.parse(json).getAsJsonObject();
        for (int i = 0; i < value.length; i++) {
            values[i] = mJson.get(value[i]).getAsString();
            System.out.println(values[i]);
        }
        return values;
    }
}
