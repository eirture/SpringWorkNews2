package com.springapp.mvc.commont;

import com.google.gson.Gson;

/**
 * Created by Jie on 2015-12-11.
 */
public class ResponseTools {

    public static String bean2Json(Object o) {
        Gson gson = new Gson();
        return gson.toJson(o);
    }
}
