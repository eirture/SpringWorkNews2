package com.springapp.mvc.commont;

/**
 * Created by Jie on 2015-12-01.
 */
public class Constant {

    public final static String USER = "user";

    /*logining*/
    public final static String EMAIL = "email";
    public final static String USER_NAME = "username";
    public final static String PASSWD = "passwd";

    public final static String ICON = "icon";

    public static final String CURRENT_USER_ID = "CURRENT_USER_ID";

    public static final String USER_ID = "userid";

    //Article
    public final static String TITLE = "title";
    public final static String CONTENT = "content";
    public final static String ARTICLE_ID = "articleid";
    public final static String ARTICLE_TITLE = "title";
    public final static String IS_PASS = "ispass";
    public final static String PASS = "pass";

    public final static String ID_SAVE = "id_translation";

    //Manager
    public final static String MANAGER = "manager";


}
