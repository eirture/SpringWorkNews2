package com.springapp.mvc.commont;

import java.util.List;

/**
 * Created by Jie on 2015-12-08.
 */
public class ListUtils {

    public static boolean isEmpty(List list) {
        return list == null || list.size() == 0;
    }
}
