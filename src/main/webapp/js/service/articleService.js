(function() {
	angular.module('app')
		.service('articleService', ['$http', articleService]);

	function articleService($http){
		this.currentArticleId = -1;
		this.allArticleNum = -1;
		this.passArticleNum = -1;
		this.unpassArticleNum = -1;
		this.getArticleByTime = function(time){
			console.log('hello service');
			return $http({
				method: 'POST',
				data: time,
				url: "http://localhost:8080/article/findA4Time"
			});
		};
		this.checkArticle = function(articleid,ispass){
			var data = {
				articleid : articleid,
				ispass : ispass
			};
			return $http({
				method:'POST',
				data:data,
				url:"http://localhost:8080/article/pass"
			});
		}
		this.findArticle = function(articleid){
			return $http({
				method:'GET',
				url:"http://localhost:8080/article//findArticle/"+articleid
			});
		}
	}
})();