/**
 * Created by Administrator on 2015/12/16.
 */
(function(){
    angular.module('homeApp')
    .service('homeArticleService',homeArticleService);
    function homeArticleService($http){

        this.getArticleByTime = function(time){
            return $http({
                method: 'POST',
                data: time,
                url: "http://localhost:8080/article/findA4Time"
            });
        };
        this.findArticleById = function(articleid){
            return $http({
                method:'GET',
                url:"http://localhost:8080/article/findArticle/"+articleid
            });
        }
        this.findArticleByPage = function(page){
            return $http({
                method:'GET',
                url:"http://localhost:8080/article/findArticle/getPageArticle/"+page
            });
        }
        this.findArticleByType = function(type){
            var data = {
                classify : "'"+type+"'"
            }
            return $http({
                method : 'POST',
                data : data,
                url: "http://localhost:8080/article/getArticleByClassify"
            })
        }
        this.getAllType = function(){
            return $http({
               method : 'GET',
               url: "http://localhost:8080/article/allclassify"
            });
        }
    }
})();