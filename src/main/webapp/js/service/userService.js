(function() {
    angular.module('app')
        .service('userService', ['$q','$http', userService]);

    function userService($q,$http){
        this.user ;
        this.getUser = function(){
            return $http({
                method: 'GET',
                url: "http://localhost:8080/user/islogin"
            });
        };
        this.getUserById = function(userid){
            var deferred = $q.defer();
            $http({
                method:'GET',
                url:"http://localhost:8080/user/equary/"+userid
            }).success(function(data, status, headers, config){
                deferred.resolve(data.data);
            }).error(function(data, status, headers, config){
                deferred.reject('error');
            });
            return deferred.promise;
        }
        this.checkArticle = function(articleid,ispass){
            var data = {
                articleid : articleid,
                ispass : ispass
            };
            return $http({
                method:'POST',
                data:data,
                url:"http://localhost:8080/article/pass"
            });
        }
    }
})();