/**
 * Created by guoyang on 2015/12/6.
 */
var app = angular.module("myApp", []);
app.controller("validateCtrl", function ($scope) {
    //$scope.email = "15717915062@163.com";
    //$scope.psw = "111111";
    $scope.isErrorPsw = false;

    $scope.isErrorEmail = false;
    $scope.isRequired = false;
    $scope.isNotEmail = false;

    $scope.blurEmail = function () {

        if ($scope.myForm.email.$dirty && $scope.myForm.email.$invalid) {
            if ($scope.myForm.email.$error.required) {
                $scope.isRequired = true;
            }
            if ($scope.myForm.email.$error.email) {
                $scope.isNotEmail = true;
            }
        }
        $scope.isErrorEmail = $scope.isRequired || $scope.isNotEmail;
    };
    $scope.focusEmail = function () {
        $scope.isErrorEmail = false;
        $scope.isNotEmail = false;
        $scope.isRequired = false;
    }
    $scope.blurPsw = function () {
        $scope.isErrorPsw = ($scope.myForm.psw.$dirty) && ($scope.myForm.psw.$invalid);
    };
    $scope.focusPsw = function () {
        $scope.isErrorPsw = false;
    }
});