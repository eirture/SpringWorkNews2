/**
 * Created by guoyang on 2015/12/12.
 */

var app = angular.module("homeApp");

app.controller("validateController", function ($scope, $http, $location) {

    $scope.isErrorPsw = false;
    $scope.isErrorEmail = false;
    $scope.isRequired = false;
    $scope.isNotEmail = false;
    $scope.isDisable = true;
    $scope.isLoginError = false;

    $scope.blurEmail = function () {

        if ($scope.myForm.email.$dirty && $scope.myForm.email.$invalid) {
            if ($scope.myForm.email.$error.required) {
                $scope.isRequired = true;

            }
            if ($scope.myForm.email.$error.email) {
                $scope.isNotEmail = true;
            }
        }
        $scope.isErrorEmail = $scope.isRequired || $scope.isNotEmail;
        $scope.isDisable = $scope.isErrorEmail || $scope.isErrorPsw;
    };
    $scope.focusEmail = function () {
        $scope.isErrorEmail = false;
        $scope.isNotEmail = false;
        $scope.isRequired = false;

    }
    $scope.blurPsw = function () {
        $scope.isErrorPsw = ($scope.myForm.psw.$dirty) && ($scope.myForm.psw.$invalid);
        $scope.isDisable = $scope.isErrorEmail || $scope.isErrorPsw;
    };
    $scope.focusPsw = function () {
        $scope.isErrorPsw = false;

    }

    $scope.validate = function () {

        $http.post('/user/login', {username: $scope.email, passwd: $scope.psw}
        ).success(function (data, status, headers, config) {

            var isPass = data.status;
            if (isPass == 'pass') {
                //链接跳转
                alert("登录成功!")
                window.location.href = "../news/mainPage";
            }
            else {
                alert("账号或者密码错误");
            }


        }).error(function (data, status, headers, config) {
            //处理错误
            alert("请求超时");
        });
    }

});
app.controller('signCtrl', function ($scope, $http) {

    $scope.islogin = true;

    $http.get('/user/islogin').success(function (data, status, headers, config) {
        $scope.islogin = true;
        if (!$scope.$$phase) {
            $scope.$apply();
        }

        if (data.status == 'pass') {
            $scope.islogin = false;
            $("#head-icon")[0].src = "../../" + data.data.icon;
            $scope.username = data.data.username;
        }

    }).error(function (data, status, headers, config) {

    });
});
