/**
 * Created by guoyang on 2015/12/15.
 */
var app=angular.module("homeApp");
app.controller("searchController",function($scope){

    //json数组
    var pagedatas=[
        {
            "articleId":8,
            "userid":1,
            "content":"Decking out your smartphone for Christmas has opened up another niche market for mobile app designers hoping to cash in on a Santa-style windfall.",
            "createTime":"Dec 12, 2015 3:51:27 PM",
            "alterTime":"Dec 12, 2015 3:51:27 PM",
            "title":"江西财经大学",
            "pass":0,
            "classify":"inland"
        },
        {
            "articleId":9,
            "userid":1,
            "content":"Things were going so well for Xiaomi Corp. Customers were lining up, investors were swooning and the Beijing-based startup closed funding at a $45 billion valuation. That was last year.",
            "createTime":"Dec 12, 2015 4:17:10 PM",
            "alterTime":"Dec 12, 2015 4:17:10 PM",
            "title":"吹啊吹啊无所谓扰乱我",
            "pass":0,
            "classify":"inland"
        },
        {
            "articleId":10,
            "userid":1,
            "content":"Bu Wenting, a stewardess of China United Airlines, greets passengers before they board the plane at the Shanghai Hongqiao Airport.[Provided to China Daily]",
            "createTime":"Dec 14, 2015 8:19:27 PM",
            "alterTime":"Dec 14, 2015 8:19:31 PM",
            "title":"看我勇敢的在微笑",
            "pass":0,
            "classify":"inland"
        },
        {
            "articleId":11,
            "userid":1,
            "content":"A photo illustration shows a $100 banknote placed above Chinese 100 yuan banknotes in Beijing in this May 10, 2013 file photo. [Photo/Agencies]",
            "createTime":"Dec 14, 2015 8:21:52 PM",
            "alterTime":"Dec 14, 2015 8:21:55 PM",
            "title":"Chinese yuan weakens to 4-year",
            "pass":1,
            "classify":"inland"
        },
        {
            "articleId":12,
            "userid":1,
            "content":"China may step up efforts to slash its mounting housing inventory, with farmers and migrant workers being the biggest beneficiaries.",
            "createTime":"Dec 22, 2015 8:40:12 PM",
            "alterTime":"Dec 14, 2015 8:40:17 PM",
            "title":"Housing subsidies may be on the way",
            "pass":1,
            "classify":"inland"
        },
        {
            "articleId":14,
            "userid":1,
            "content":"Elves disguised as middlemen snap up electronic toys, giving a boost to exports in 2015",
            "createTime":"Dec 14, 2015 8:42:02 PM",
            "alterTime":"Dec 14, 2015 8:42:04 PM",
            "title":"Santa's workshop in Yiwu goes electric",
            "pass":1,
            "classify":"inland"
        },
        {
            "articleId":28,
            "userid":1,
            "content":"Decking out your smartphone for Christmas has opened up another niche market for mobile app designers hoping to cash in on a Santa-style windfall.",
            "createTime":"Dec 12, 2015 9:12:15 AM",
            "alterTime":"Dec 1, 2015 9:12:29 AM",
            "title":"China's journey through green trains",
            "pass":1,
            "classify":"inland"
        },
        {
            "articleId":29,
            "userid":1,
            "content":"Decking out your smartphone for Christmas has opened up another niche market for mobile app designers hoping to cash in on a Santa-style windfall.",
            "createTime":"Dec 10, 2015 9:13:03 AM",
            "alterTime":"Dec 9, 2015 9:13:24 AM",
            "title":"Please Dress Me in ",
            "pass":1,
            "classify":"inland"
        },
        {
            "articleId":30,
            "userid":1,
            "content":"Decking out your smartphone for Christmas has opened up another niche market for mobile app designers hoping to cash in on a Santa-style windfall.",
            "createTime":"Dec 11, 2015 9:13:35 AM",
            "alterTime":"Dec 15, 2015 9:13:44 AM",
            "title":"Housing subsidies may be on ",
            "pass":0,
            "classify":"inland"
        },
        {
            "articleId":31,
            "userid":1,
            "content":"Decking out your smartphone for Christmas has opened up another niche market for mobile app designers hoping to cash in on a Santa-style windfall.",
            "createTime":"Dec 11, 2015 9:14:02 AM",
            "alterTime":"Dec 5, 2015 9:13:50 AM",
            "title":"Chinese yuan weakens to 4-",
            "pass":1,
            "classify":"inland"
        },
        {
            "articleId":32,
            "userid":1,
            "content":"Decking out your smartphone for Christmas has opened up another niche market for mobile app designers hoping to cash in on a Santa-style windfall.",
            "createTime":"Dec 15, 2015 9:15:08 AM",
            "alterTime":"Dec 10, 2015 9:14:39 AM",
            "title":"Chinese yuan weakens to 4-",
            "pass":0,
            "classify":"inland"
        },
        {
            "articleId":33,
            "userid":1,
            "content":"Decking out your smartphone for Christmas has opened up another niche market for mobile app designers hoping to cash in on a Santa-style windfall.",
            "createTime":"Dec 12, 2015 9:15:18 AM",
            "alterTime":"Dec 12, 2015 9:15:24 AM",
            "title":"Liu Shishi's wax figure revealed in ",
            "pass":1,
            "classify":"inland"
        },
        {
            "articleId":34,
            "userid":1,
            "content":"Decking out your smartphone for Christmas has opened up another niche market for mobile app designers hoping to cash in on a Santa-style windfall.",
            "createTime":"Dec 11, 2015 9:15:30 AM",
            "alterTime":"Dec 13, 2015 9:15:36 AM",
            "title":"Exploring China's Longmen Gro",
            "pass":1,
            "classify":"inland"
        },
        {
            "articleId":35,
            "userid":9,
            "content":"ssss",
            "createTime":"Dec 15, 2015 4:39:24 PM",
            "alterTime":"Dec 15, 2015 4:39:24 PM",
            "title":"Test",
            "pass":0,
            "classify":"inland"
        },
        {
            "articleId":36,
            "userid":9,
            "content":"##?? ![p](http://p1.img.cctvpic.com/photoAlbum/page/performance/img/2015/12/15/1450131653912_720.jpg)",
            "createTime":"Dec 15, 2015 4:43:10 PM",
            "alterTime":"Dec 15, 2015 4:43:10 PM",
            "title":"??",
            "pass":0,
            "classify":"inland"
        }
    ];
    $scope.quaryList=pagedatas;
    $scope.show=false;


    $scope.blurQ= function () {

        if($scope.quaryForm.key.$error.required){    //表示未填写
            $scope.show=false;
        }
        else $scope.show=true;

    };

    $scope.con=function(){
        $scope.show=true;
    }

});
