/**
 * Created by guoyang on 2015/12/17.
 */
(
    function(){
        angular.module("homeApp")
            .controller("validateController",['$scope','validateService',
               validateController
            ]);
        function validateController($scope,validateService){

            $scope.isErrorPsw = false;
            $scope.isErrorEmail = false;
            $scope.isRequired = false;
            $scope.isNotEmail = false;
            $scope.isDisable=true;
            $scope.isLoginError=false;

            $scope.blurEmail = function () {

                if ($scope.myForm.email.$dirty && $scope.myForm.email.$invalid) {
                    if ($scope.myForm.email.$error.required) {
                        $scope.isRequired = true;

                    }
                    if ($scope.myForm.email.$error.email) {
                        $scope.isNotEmail = true;
                    }
                }
                $scope.isErrorEmail = $scope.isRequired || $scope.isNotEmail;
                $scope.isDisable=$scope.isErrorEmail||$scope.isErrorPsw;
            };
            $scope.focusEmail = function () {
                $scope.isErrorEmail = false;
                $scope.isNotEmail = false;
                $scope.isRequired = false;

            }
            $scope.blurPsw = function () {
                $scope.isErrorPsw = ($scope.myForm.psw.$dirty) && ($scope.myForm.psw.$invalid);
                $scope.isDisable=$scope.isErrorEmail||$scope.isErrorPsw;
            };
            $scope.focusPsw = function () {
                $scope.isErrorPsw = false;

            }


            $scope.validate=function(){
                var user={username:$scope.email,passwd:$scope.psw};
                validateService.findUser(user).success(function(data,status,headers,config){
                    var isPass=data.status;
                    alert("status:" + isPass);
                    if(isPass == 'pass'){
                        //链接跳转
                        window.location.href = "../news/main";
                    }
                    else{
                        alert("账号或者密码错误");
                    }
                }).error(function(data,status,headers,config){
                    //处理错误
                    alert("请求超时");
                });
            }
        }
    }
)