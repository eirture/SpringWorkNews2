(function() {

	angular.module("app", ['ngMaterial'])
		.controller("userController", ['$scope', '$mdDialog','userService',
			userController
		]);

	function userController($scope, $mdDialog,userService) {
		$scope.userIcon;
		$scope.logout = function(ev) {
			alert = $mdDialog.confirm({
				title: '提示',
				content: '您是否要登出账号?',
				ok: '确定',
				cancel: '取消'
			});
			$mdDialog
				.show(alert)
				.then(function() {
					window.location.href="http://localhost:8080/";
				}, function() {
					console.log('logout cancel');
				})
				.finally(function() {
					alert = undefined;
				});
		};
		userService.getUser()
			.success(function(data,status,headers,config){
				$scope.user = data.data;
				$scope.userIcon = $scope.user.icon;
			}).error(function(data,status,headers,config){
				showSimpleToast("网络错误");
			});
	}
})();