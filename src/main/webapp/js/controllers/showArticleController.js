(function () {
    var testEditor;

    var app =  angular.module("article",[]);
    app.controller('showArticleController',['$scope','$http',showArticleController]);

    function showArticleController($scope,$http){
        var articleid ;
        $scope.article;
        $scope.user;

        var url=location.href;
        var tmp1=url.split("?")[1];
        var tmp2=tmp1.split("&")[0];
        var tmp3=tmp2.split("=")[1];
        articleid=tmp3;
        console.log("articleid ="+articleid);

        $http({
            method:'GET',
            url:"http://localhost:8080/article//findArticle/"+articleid
        }).success(function(data,status,headers,config){
            $scope.article = data.data;
            console.log($scope.article.content);
            testEditor = editormd("test-editormd", {
                width: "90%",
                height: 640,
                markdown:$scope.article.content,
                path: "../../../framework/lib/",
                readOnly: true,
                styleActiveLine: false,
                onload: function () {
                    this.previewing();
                }
            });
            $http({
                method:'GET',
                url:"http://localhost:8080/user/equary/"+$scope.article
            }).success(function(data, status, headers, config){
                $scope.user = data.data;
            }).error(function(data, status, headers, config){
                alert("获取作者失败");
            });

        }).error(function(data,status,headers,config){
            alert("获取文章失败");
        });

    }


    testEditor
})();