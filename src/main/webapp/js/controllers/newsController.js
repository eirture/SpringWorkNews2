(function() {
	angular.module('app')
		.controller('newsController', ['$scope', '$http','$timeout','$mdToast','userService','articleService', newsController]);

	function newsController($scope, $http,$timeout,$mdToast,userService,articleService) {
		$scope.showState = false;
		$scope.articles;
		$scope.users = new Array();
		$scope.allnum = 'loading';
		$scope.passnum = 'loading';
		$scope.iconnum = 0;
		$scope.pagenum = 1;
		$scope.allpagenum = new Array;
		$scope.articlePage;
		$scope.time = {
			begin: '2015-10-10 00:00:00',
			end: '2015-12-17 23:59:59'
		};
		$scope.lastPage = function(){
			if($scope.pagenum != 1) {
				$scope.pagenum = $scope.pagenum - 1;
				$scope.articlePage = getArticleByPage($scope.pagenum);
			}
		};
		$scope.nextPage = function(){
			if($scope.pagenum != $scope.allpagenum.length) {
				$scope.pagenum = $scope.pagenum + 1;
				$scope.articlePage = getArticleByPage($scope.pagenum);
			}
		}
		$scope.pointPage = function(num){
			$scope.articlePage = getArticleByPage(num);
			$scope.pagenum = num;
			//getdata();
		}
		var getArticleByPage = function(pagenum){
			var arr =  $scope.articles.slice((pagenum-1)*5,pagenum*5);
			$scope.iconnum =arr.slice(0,5);
			$scope.users = [];
			for(var j = 0 ;j<$scope.iconnum.length;j++){
				var promise = userService.getUserById(arr[j].userid);
				promise.then(function(result){
					$scope.users.push(result);
				},function(error){
					console.log("获取第"+j+"个头相出错");
				});
			}
			return arr;
		};

		var showSimpleToast = function(message) {
			alert(message);
		};
		var getdata = function(){
			articleService.getArticleByTime($scope.time)
				.success(function(data, status, headers, config) {
					$scope.articles=data.data;
					var num = Math.ceil( $scope.articles.length / 5);
					for(var i=0;i<num;i++){
						$scope.allpagenum.push(i);
					}
					$scope.articlePage = getArticleByPage($scope.pagenum);
					if($scope.articles.length >8){
						$scope.iconnum = $scope.articles.slice(0,5);

					}else{
						$scope.iconnum = $scope.articles;
					}
					$scope.allnum = $scope.articles.length;
					var ment_num = 0;
					for(var i = 0;i < $scope.articles.length;i++){
						if($scope.articles[i].pass == 1){
							ment_num = ment_num +1;
						}
					}
					$scope.passnum = ment_num;
					console.log(data);
					console.log($scope.articles);
					console.log($scope.passnum+"-"+$scope.allnum);
					if(!$scope.$$phase){
						$scope.$apply();
					}
				}).error(function(data, status, headers, config) {
					//处理错误
					showSimpleToast("网络错误");
				});
		}
		getdata();

		$scope.checkArticle = function(articleid,ispass){
			articleService.checkArticle(articleid,ispass)
			.success(function(data,status,headers,config){
				if(data.status == 'pass'){
					showSimpleToast("提交成功");
					getdata();
				}else{
					showSimpleToast("您没有权限操作");
				}
			}).error(function(data,status,headers,config){
					showSimpleToast("网络错误");
			});
		}

		$scope.newsClick = function() {
			$scope.showState = !$scope.showState;
		}
	}
})();