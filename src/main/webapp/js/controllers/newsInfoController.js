/**
 * Created by Yi on 2015/12/14.
 */
angular.module('app')
    .controller('AppCtrl', function($scope, $mdDialog, $mdMedia,articleService) {
        $scope.status = '  ';
        $scope.customFullscreen = $mdMedia('sm');
        $scope.showAlert = function(ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            // Modal dialogs should fully cover application
            // to prevent interaction outside of dialog
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('This is an alert title')
                    .textContent('You can specify some description text in here.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Got it!')
                    .targetEvent(ev)
            );
        };
        $scope.showConfirm = function(ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete your debt?')
                .textContent('All of the banks have agreed to forgive you your debts.')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Sounds like a scam');
            $mdDialog.show(confirm).then(function() {
                $scope.status = 'You decided to get rid of your debt.';
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        };
        $scope.showAdvanced = function(ev,articleId) {
            articleService.currentArticleId = articleId;
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'newsInfo',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: $mdMedia('sm') && $scope.customFullscreen
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('sm');
            }, function(sm) {
                $scope.customFullscreen = (sm === true);
            });
        };
        $scope.showTabDialog = function(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '../../view/newsInfo.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };
    });
function DialogController($scope, $mdDialog,articleService) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
        if(answer == 'pass'){
            checkArticle(articleService.currentArticleId,1);
        }else{
            checkArticle(articleService.currentArticleId,0);
        }
    };
    $scope.article = 'Loading';

    articleService.findArticle(articleService.currentArticleId)
        .success(function(data,status,headers,config){
            $scope.article = data.data;
        }).error(function(data,status,headers,config){
            alert("网络错误");
        });
    var checkArticle = function(articleId,ispass){
        articleService.checkArticle(articleId,ispass)
            .success(function(data,status,headers,config){
                if(data.status == 'pass'){
                    showSimpleToast("提交成功");
                    getdata();
                }else{
                    alert("您没有权限操作");
                }
            }).error(function(data,status,headers,config){
                alert("网络错误");
            });
    }

}