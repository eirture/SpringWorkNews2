/**
 * Created by Administrator on 2015/12/16.
 */
(function(){
    var app = angular.module("homeApp", []);
        app.controller('homeArticleController',['$scope','homeArticleService',homeArticleController]);
    function homeArticleController($scope,homeArticleService){
        var allArticles;
        $scope.currentPage = 0;
        $scope.articles = new Array();
        $scope.types;                  //{'XX','XX','XX','XX'}
        $scope.pages = new Array();    //{1,2,3,4,5}
        $scope.currentType = 'inland';


        $scope.clickType = function(type){
            getArticlByType(type);

        }
        $scope.clickPage = function(page){
            getArticlebyPage(page);
        }
        $scope.lastPage = function() {
            if ( $scope.currentPage > 0) {
                getArticlebyPage( $scope.currentPage -1);
            }
        };
        $scope.nextPage = function(){
            if( $scope.pages.length >  $scope.currentPage + 1){
                getArticlebyPage( $scope.currentPage + 1);
            }
        }
        $scope.openArticle = function(articleid){
            var url = "http://localhost:8080/news/articleh/show-article";
            window.location.assign(url+"?"+"articleid="+articleid);
        }
        var init = function(){
            //homeArticleService.getAllType()
            //    .success(function(data,status,headers,config){
            //        $scope.types = data.data;
            //        $scope.currentType = $scope.types[0];
            //    }).error(function(data,status,headers,config){
            //        alert("网络连接中断");
            //    });
            homeArticleService.findArticleByType($scope.currentType)
                .success(function(data,status,headers,config){
                    allArticles = data.data;
                    console.log(data);
                    getArticlebyPage( $scope.currentPage);
                    getPageNum();
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                }).error(function(data,status,headers,config){
                    alert("网络连接中断");
                });
        }

        init();

        var getArticlByType = function(type){
            $scope.currentType = type;
            $scope.currentPage = 0;
            homeArticleService.findArticleByType($scope.currentType)
                .success(function(data,status,headers,config){
                    allArticles = data.data;
                    getArticlebyPage(0);
                    getPageNum();
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                }).error(function(data,status,headers,config){
                    alert("网络连接中断");
                });
        }
        var getArticlebyPage = function(page){
            $scope.currentPage = page;
            $scope.articles = allArticles.slice( $scope.currentPage*5, $scope.currentPage*5+5);
        }
        var getAllType = function(){
            homeArticleService.getAllType()
                .success(function(data,status,headers,config){
                    $scope.types = data.data;
                    $scope.currentType = $scope.types[0];
                }).error(function(data,status,headers,config){
                    alert("网络连接中断");
                });
        }
        var getPageNum = function () {
            $scope.pages = new Array();
            var num = Math.ceil( allArticles.length / 5);
            for(var i = 0 ; i < num ; i++){
                $scope.pages.push(i);
            }
        }
    }

})();