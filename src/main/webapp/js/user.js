/**
 * Created by Jie on 2015-12-17.
 */
function loginout() {
    $.get("../../user/loginout", function (data, status) {
        console.log(status);
        console.log(data.status + "," + data.data);
        if (data.status) {
            alert(data.data);
            location.reload();
        }
    }, "json");
}

<!--文件上传 -->
function upload() {
    console.log("upload js");
    upload_orgin(function (data, status) {
        if (data.status) {
            $("#head-icon").attr('src', '../../' + data.data);
        } else {
            alert("[保存失败！]");
        }
    });
    return false;
}

function upload_orgin(callback) {
    $.ajaxFileUpload
    (
        {
            url: '../../../upload/uploadfile',
            secureuri: false,
            fileElementId: 'files',
            dataType: 'json',
            success: callback,
            error: function (data, status, e) {
                alert("【服务器异常，请连续管理员！】" + e);
            }
        }
    );
}


function getUser(callback) {

    $.get('../../user/getCurrentUser', function (data, status) {
        //console.log(data.data);
        if (data.status) {
            callback(data.data);
        }
    }, "json");
}
